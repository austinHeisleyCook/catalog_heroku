/*******************************************************************************
 * COPYRIGHT © 2018 @CODERPILLR
 * @author Ciel
 * @modified 2/18/2018 8:53
 ******************************************************************************/
const
  PORT              = require('./port')
;
/**
 * Event listener for HTTP server "listening" event.
 */
const onListening = () => {
  console.log('Listening on ' + PORT);
};

module.exports = onListening;
