/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 1:22
 ******************************************************************************/
const
  fs     = require('fs')
  , YAML = require('js-yaml')
;

class Yaml {
  static parse(text, name) {
    name        = name || '__content';
    let re      = /^(-{3}(?:\n|\r)([\w\W]+?)(?:\n|\r)-{3})?([\w\W]*)*/
      , results = re.exec(text)
      , conf    = {}
      , yamlOrJson;

    if (yamlOrJson = results[2]) {
      if (yamlOrJson.charAt(0) === '{') {
        conf = JSON.parse(yamlOrJson);
      } else {
        conf = YAML.load(yamlOrJson);
      }
    }

    conf[name] = results[3] ? results[3] : '';

    return conf;
  }

  static front(context, name) {
    let contents;
    if (fs.existsSync(context)) {
      contents = fs.readFileSync(context, 'utf8');
      if (contents instanceof Error) return contents;
      return this.parse(contents, name);
    } else if (Buffer.isBuffer(context)) {
      return this.parse(context.toString(), name);
    } else {
      return this.parse(context, name);
    }
  }
}

module.exports = Yaml;
