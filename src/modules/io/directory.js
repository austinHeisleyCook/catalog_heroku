/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 0:54
 ******************************************************************************/
const
  fs     = require('fs')
  , path = require('path')
;

class Directory {
  static metadata(dir) {
    const data = fs.readFileSync(path.join(dir, 'index.json'));
    return JSON.parse(data);
  }
}

module.exports = Directory;
