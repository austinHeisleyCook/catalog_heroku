/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 1:26
 ******************************************************************************/
const sort = (a, b) => {
  // if a.order is -1
  return a.order < 0
    // and b.order is -1
         ? b.order < 0
           // alphabetic sort
           ? a.title > b.title
           // if b.order is not -1
           // then lowest order
           : b.order > a.order
    // if a.order is not -1
         : b.order < 0
           // then the highest number wins
           ? a.order > b.order
           : b.order < a.order;
};

module.exports = sort;
