/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 0:52
 ******************************************************************************/
const sanitize = (args, replacement = '/') => {
  if (args.length > 1)
    return args.join(replacement)
    .replace(/^\//, '')
    .replace(/$\//, '');
  return args;
};

module.exports = sanitize;
