/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 1:4
 ******************************************************************************/
const
  fs          = require('fs')
  , path      = require('path')
  , YAML      = require('js-yaml')
;
const
  Directory   = require('./directory')
  , File      = require('./file')
  , Converter = require('@coderpillr/showdown/converter')
;

const
  data       = require('./data')
  , sanitize = require('./sanitize')
;

class Files {
  constructor(site) {
    console.log('creating filesystem: ', site);
    this.save(site, `./${site}/pages/`);
  }

  build(site, current, extensions, parent = '', title, url, content, group, nest, icon = null, order = -1) {
    if (parent === '' && current === './public/pages/' || parent === '' && current === './staff/pages/') {
      content = '.yaml';
    }

    // construct an object to contain results
    const result = {
      path   : current,
      url    : url,
      icon   : icon,
      title  : title,
      parent : parent,
      order  : order,
      group  : group,
      nest   : nest,
      content: content
               ? sanitize([
          current.split(/[\\,/]+/).slice(0, -1).join('/'),
          content
        ], '/')
               : undefined,
      //: parent === '' ? `${site}/pages/index.yaml` : undefined,
      items  : []
    };

    let __pathdata, __metadata;
    // try to read the root directory first.
    try {
      __pathdata = fs.readdirSync(current);
    } catch (ex) {
      // if the user does not have access to the directory,
      // then throw an error.
      if (ex.code === 'EACCES') return null;
    }
    // if there is no directory, return nothing.
    if (__pathdata === undefined) return null;

    // flatten the results into an array
    __pathdata = __pathdata.map(child => path.join(current, child));
    // as long as there are files or
    // directories to read, continue
    for (let i = 0; i < __pathdata.length; i++) {
      // hold the stats and current directory items
      let stats;
      const item = __pathdata[i];

      // get the stats for the current directory
      try {
        stats = fs.statSync(item);

      } catch (ex) {
        return null;
      }
      if (stats.isFile() && path.extname(item) === '.yaml') {
        // get the file's metadata
        const metadata = File.metadata(item);

        result.items.push({
          path     : item,
          title    : metadata.title.toUpperCase(),
          extension: path.extname(item),
          parent   : parent,
          order    : metadata.order ? metadata.order : -1,
          group    : metadata.group ? metadata.group.toUpperCase() : null,
          nest     : metadata.nest ? metadata.nest.toUpperCase() : null,
          icon     : metadata.icon ? metadata.icon.toLowerCase() : null,
          url      : sanitize([
            url,
            metadata.url
          ])
          //url      : `${url}/${metadata.url}`,
        });
      }
      if (stats.isDirectory()) {
        // try to read the json file for the directory
        __metadata       = Directory.metadata(item);
        // current, extensions, parent = '', title, url, content, order = -1
        const walkResult = this.build(
          site,
          item,
          extensions,
          sanitize([
            parent,
            __metadata.url
          ]),
          __metadata.title.toUpperCase(),
          sanitize([
            result.parent,
            __metadata.url
          ]),
          __metadata.content,
          __metadata.group ? __metadata.group.toUpperCase() : null,
          __metadata.nest ? __metadata.nest.toUpperCase() : null,
          __metadata.icon ? __metadata.icon.toLowerCase() : null,
          __metadata.order);
        if (walkResult !== null) {
          result.items.push(walkResult);
        }
      }
    }
    return result;
  }

  save(site, filepath) {
    const result = this.build(site, filepath);
    fs.writeFileSync(
      path.join(process.cwd(), `index.${site}.json`),
      JSON.stringify(result, null, 2));
    return result;
  }

  search(data, key, value, results = {}) {
    if (!value) {
      return results = {
        data: {
          items: []
        }
      };
    }
    if (data[key] === value) {
      results = {
        match: true,
        data : data
      };
    } else {
      Object
      .entries(data)
      .reduce((current, [k, v]) =>
        Array.isArray(v) ? v.map(e =>
                           this.search(e, key, value))
                            .filter(result => result.match)
                            .filter(result => result.data != null)
                            .map(result => results = Object.assign({}, result, results))
                         : {match: false}, {});
    }
    return results;
  }


  parse(regex, input) {
    return input.split(regex)
                .map(data => {
                  let output;

                  try {
                    output = YAML.safeLoad(data);
                  } catch (ex) {
                    output = null;
                  }

                  return {
                    output: output,
                    input : data
                  };
                })
                .map(data => {
                  return data;
                })
                .map(data => {
                  if (typeof data.output !== 'object' || data.output === null) {
                    return {
                      component: 'markdown',
                      content  : Converter.makeHtml(data.input.replace(/^<-- (.*) -->|<-- END -->$/gmi, ''))
                    };
                  } else {
                    return {
                      component: data.output.component,
                      content  : data.output
                    };
                  }
                })
                .map(data => data);
  }

  read(context) {
    // hold the contents of the read operation
    let contents;

    // make sure the file exists
    if (fs.existsSync(context)) {
      // try to read the file
      contents = fs.readFileSync(context, 'utf8');

      // make sure the contents are not malformed
      if (contents instanceof Error) {
        return null;
      }

      return contents
      .split(/^---$/gmi)
      .slice(2)
      .map(data => {
        let output;

        try {
          output = YAML.safeLoad(data);
        } catch (ex) {
          output = null;
        }

        return {
          output: output,
          input : data
        };
      })
      .map(data => {
        // console.log('---');
        // console.log(data);
        return data;
      })
      .map(data => {
        if (typeof data.output !== 'object' || data.output === null) {
          // see if the content is a collapsible
          //const regex   = /<-- (.*) -->([\S\s]*?)<-- END -->/gmi;
          const regex   = /\+!\[(.*?)\][\r\n]+(.*?)[\r\n].*?/gmi;
          const matches = regex.exec(data.input) || [];

          // console.log('[data]: ', JSON.stringify(data.input, null, 2));

          if (matches.length > 0) {
            return {
              component: 'collapse',
              title    : matches[1],
              //content: this.parse(/\+!\[(.*?)\][\r\n]+/gmi, data.input) || data.input
              //content  : this.parse(matches[2] || data.input) || data.input
              content  : this.parse(
                /(?:<\+\+>)([\S\s]*?)(?:<\+\+>)/gmi,
                data.input.replace(/\+!\[(.*?)\][\r\n]+/gmi, '')
              ) || data.input
            };
          } else {
            return {
              component: 'markdown',
              content  : Converter.makeHtml(data.input)
              // content  : Converter.makeHtml(data.input.replace(/^<-- (.*) -->|<-- END -->$/gmi, ''))
            };
          }
        } else {
          return {
            component: data.output.component,
            content  : data.output
          };
        }
      })
      .map(data => data);
    }
  }
}

module.exports = Files;
