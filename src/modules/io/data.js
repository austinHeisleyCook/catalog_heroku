/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 1:44
 ******************************************************************************/
const
  fs       = require('fs')
  , path   = require('path')
;
const data = (site = 'public') => {
  console.log(`reading ${site} data`);
  const data = fs.readFileSync(path.join(process.cwd(), `index.${site}.json`));
  return JSON.parse(data);
};

module.exports = data;
