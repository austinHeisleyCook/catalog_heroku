/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 1:25
 ******************************************************************************/
const distinct = (myArr, prop) => {
  return myArr !== null ? myArr.filter((obj, pos, arr) => {
    return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
  }) : [];
};

module.exports = distinct;
