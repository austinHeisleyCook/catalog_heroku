/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 0:54
 ******************************************************************************/
const
  yaml = require('@coderpillr/yaml')
;

class File {
  static metadata(context) {
    return yaml.front(context);
  }
}

module.exports = File;
