/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 11:44
 ******************************************************************************/
const extend = (showdown) => {
  showdown.setFlavor('github');
  const converter = new showdown.Converter();
  showdown.extension('sup', () => [
    {
      type  : 'lang',
      filter: text => text.replace(
        /^(?:\s\^{1})([^\^].*)(?:\^)(?:\t|\s{2,4})+$/gmi,
        (...params) => {
          return params[ 0 ].replace(/^(?:\s\^{1})([^\^].*)(?:\^)(?:\t|\s{2,4})+$/gmi, `<sup class='fxBlock'>${ params[ 1 ] }</sup>`);
        }
      )
    },
    {
      type  : 'lang',
      filter: text => text.replace(
        /(?:\s\^{1})([^\^].*)(?:\^)/gmi,
        (...params) => {
          return params[ 0 ].replace(/(?:\s\^{1})([^\^].*)(?:\^)/gmi, `<sup>${ params[ 1 ] }</sup>`);
        }
      )
    },
  ]);
  showdown.extension('sub', () => [
    {
      type  : 'lang',
      filter: text => text.replace(
        /^(?:\^{2})([^\^].*)(?:\^{2})(?:\t|\s{2,4})+$/gmi,
        (...params) => {
          return params[ 0 ].replace(/^(?:\^{2})([^\^].*)(?:\^{2})(?:\t|\s{2,4})+$/gmi, `<sub class='fxBlock'>${ params[ 1 ] }</sub>`);
        }
      )
    },
    {
      type  : 'lang',
      filter: text => text.replace(
        /(?:\s\^{2})([^\^].*)(?:\^{2})/gmi,
        (...params) => {
          return params[ 0 ].replace(/(?:\s\^{2})([^\^].*)(?:\^{2})/gmi, `<sub>${ params[ 1 ] }</sub>`);
        }
      )
    }
    // {
    //   type  : 'lang',
    //   filter: text => text.replace(/\s+\*([^\*].*?)\*/gmi,
    //     (...params) => {
    //       return params[ 0 ].replace(/\s+\*([^\*].*?)\*/gmi, `<em>${ params[ 1 ] }</em>`);
    //     })
    // }
  ]);
  showdown.extension('collapsible', () => [
    {
      type: 'lang',
      filter: text => text.replace(
        /\+!\[(.*)]\n===\n([^=]*)\n===/gmi,
        (...params) => {
          return params[0].replace(
            /\+!\[(.*)]\n===\n([^=]*)\n===/gmi,
            `
                        <au-view-collapse
                          title='${params[1]}'
                          data='${params[2]}'>
                        </au-view-collapse>`);
        }
      )
    }
  ]);
  showdown.extension('term', () => [
    {
      type  : 'lang',
      filter: text => text.replace(
        /->([^<-]+)<-/gmi,
        (...params) => {
          return params[ 0 ].replace(
            /->([^<-]+)<-/gmi, `<span class='au-term'>${params[1]}</span>`);
        }
      )
    }
  ]);
  showdown.extension('tags', () => [
    {
      type  : 'lang',
      filter: text => text.replace(
        /(-(&gt;|>)\((.*)?\))/gmi,
        (...params) => {
          return params[ 0 ].replace(
            /(-(&gt;|>)\((.*)?\))/gmi, `<au-view-tag name='${ params[ 3 ] }'></au-view-tag>`);
        }
      )
    }
  ]);
  showdown.extension('notices', () => [
    {
      type: 'lang',
      filter: text => text.replace(
        />!\[(\w)]\n===\n([^=]*)\n===/gmi,
        (...params) => {
          return params[0].replace(
            />!\[(\w)]\n===\n([^=]*)\n===/gmi, `<div class='notice ${params[1]}'>${converter.makeHtml(params[2])}</div>`);
        }
      )
    }
  ]);
  showdown.extension('typelift', () => [
    {
      type: 'output',
      filter: function (txt) {
        return txt;
      }
    },
    {
      type: 'output',
      filter: txt => {
        return txt.replace(/(<p>)?<p/gmi, '<p');
      }
    }
  ]);
  showdown.extension('p', () => [
    {
      type: 'output',
      regex: new RegExp(/-(&gt;|>)\[(\d)](.*)/gmi, 'gmi'),
      replace: `<p class='p$2'>$3`
    }
  ]);
};

module.exports = extend;
