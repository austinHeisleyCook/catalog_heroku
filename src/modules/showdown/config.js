/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 1:14
 ******************************************************************************/
const
  Showdown     = require('showdown')
;
const setup = (extensions) => {
  extensions(Showdown);

  const converter = new Showdown.Converter({
    underline                : true,
    tables                   : true,
    tasklists                : true,
    strikethrough            : true,
    simpleLineBreaks         : false,
    trimEachLine             : false,
    backslashEscapesHTMLTags : true,
    literalMidWordAsterisks  : true,
    literalMidWordUnderscores: true,
    noHeaderId               : true,
    simplifiedAutoLink       : true,
    smoothLivePreview        : true,
    encodeEmails             : true,
    extensions               : [
      'sup',
      'sub',
      'tags',
      'term',
      'notices',
      'p',
      'typelift'
    ]
  });

  return converter;
};

module.exports = setup;
