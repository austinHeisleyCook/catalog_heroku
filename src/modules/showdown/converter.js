/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 1:17
 ******************************************************************************/
const
  setup        = require('./config')
  , extensions = require('./extensions')
;


const Converter = setup(extensions);

module.exports = Converter;


