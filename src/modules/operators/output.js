/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 3/19/2018 1:22
 ******************************************************************************/

/**
 * Output a formatted message.
 * @param msg
 */
const output = (msg) => console.log(`--- [${msg}] ---`);

module.exports = output;

