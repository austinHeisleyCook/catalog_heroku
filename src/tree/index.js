/*******************************************************************************
 * COPYRIGHT © 2018 @CODERPILLR
 * @author Ciel
 * @modified 2/18/2018 8:53
 ******************************************************************************/


const
  FS = require('fs')
  , SHOWDOWN = require('showdown')
  , extend = require('@coderpillr/showdown/extensions')
  , PATH = require('path')
  , YAML = require('js-yaml')
  , TREE = {directory: {}, file: {}}
;


const configureShowdown = () => {
  extend(SHOWDOWN);
  return new SHOWDOWN.Converter({
    underline: true,
    tables: true,
    tasklists: true,
    strikethrough: true,
    simpleLineBreaks: false,
    trimEachLine: false,
    backslashEscapesHTMLTags: true,
    literalMidWordAsterisks: true,
    literalMidWordUnderscores: true,
    noHeaderId: true,
    simplifiedAutoLink: true,
    smoothLivePreview: true,
    encodeEmails: true,
    extensions: [
      'tags',
      'notices',
      'discord',
      'collapsible',
      'p',
      'typelift'
    ]
  });
};

const converter = configureShowdown();


TREE.build = function (site, current, extensions, parent = '', title, url, content, group, order = -1) {
  if (parent === '' && current === './public/pages/') {
    content = '.yaml';
  }
  // construct an object to contain results
  const result = {
    path: current,
    url: url,
    title: title,
    parent: parent,
    order: order,
    group: group,
    content: content
      ? sanitize([current.split(/[\\,/]+/).slice(0, -1).join('/'), content], '/')
      : undefined,
    //: parent === '' ? `${site}/pages/index.yaml` : undefined,
    items: []
  };
  let __pathdata, __metadata;
  // try to read the root directory first.
  try {
    __pathdata = FS.readdirSync(current);
  } catch (ex) {
    // if the user does not have access to the directory,
    // then throw an error.
    if (ex.code === 'EACCES') return null;
  }
  // if there is no directory, return nothing.
  if (__pathdata === undefined) return null;

  // flatten the results into an array
  __pathdata = __pathdata.map(child => PATH.join(current, child));
  // as long as there are files or
  // directories to read, continue
  for (let i = 0; i < __pathdata.length; i++) {
    // hold the stats and current directory items
    let stats;
    const item = __pathdata[i];

    // get the stats for the current directory
    try {
      stats = FS.statSync(item);

    } catch (ex) {
      return null;
    }
    if (stats.isFile() && PATH.extname(item) === '.yaml') {
      // get the file's metadata
      const metadata = TREE.file.metadata(item);

      result.items.push({
        path: item,
        title: metadata.title.toUpperCase(),
        extension: PATH.extname(item),
        parent: parent,
        order: metadata.order ? metadata.order : -1,
        group: metadata.group ? metadata.group.toUpperCase() : null,
        url: sanitize([url, metadata.url])
      });
    }
    if (stats.isDirectory()) {
      // try to read the json file for the directory
      __metadata = TREE.directory.metadata(item);
      // current, extensions, parent = '', title, url, content, order = -1
      const walkResult = TREE.build(
        site,
        item,
        extensions,
        sanitize([parent, __metadata.url]),
        __metadata.title.toUpperCase(),
        sanitize([result.parent, __metadata.url]),
        __metadata.content,
        __metadata.group ? __metadata.group.toUpperCase() : null,
        __metadata.order);
      if (walkResult !== null) {
        result.items.push(walkResult);
      }
    }
  }
  return result;
};

TREE.save = function (site, path) {
  const result = TREE.build(site, path);
  FS.writeFileSync(
    PATH.join(process.cwd(), `index.${site}.json`),
    JSON.stringify(result, null, 2));
  return result;
};

TREE.search = function (data, key, value, results = {}) {
  if (!value)
    return results = {
      data: {
        items: []
      }
    };
  if (data[key] === value) {
    results = {
      match: true,
      data: data
    };
  } else {
    Object
    .entries(data)
    .reduce((current, [k, v]) =>
      Array.isArray(v) ? v.map(e =>
          TREE.search(e, key, value))
        .filter(result => result.match)
        .filter(result => result.data != null)
        .map(result => results = Object.assign({}, result, results))
        : {match: false}, {});
  }
  return results;
};

TREE.data = function (site = 'public') {
  console.log(`reading ${site} data`);
  const data = FS.readFileSync(PATH.join(process.cwd(), `index.${site}.json`));
  return JSON.parse(data);
};

const markdown = (regex, input) => {
  return input.split(regex)
  .map(data => {
    let output;

    try {
      output = YAML.safeLoad(data);
    } catch (ex) {
      output = null;
    }

    return {
      output: output,
      input: data
    };
  })
  .map(data =>  data)
  .map(data => {
    if (typeof data.output !== 'object' || data.output === null) {
      return {
        component: 'markdown',
        content: converter.makeHtml(data.input.replace(/^<-- (.*) -->|<-- END -->$/gmi, ''))
      };
    } else {
      return {
        component: data.output.component,
        content: data.output
      };
    }
  })
  .map(data => data);
};

TREE.read = function (context) {
  // hold the contents of the read operation
  let contents;

  // make sure the file exists
  if (FS.existsSync(context)) {
    // try to read the file
    contents = FS.readFileSync(context, 'utf8');

    // make sure the contents are not malformed
    if (contents instanceof Error)
      return null;

    return contents
    .split(/^---$/gm)
    .slice(2)
    .map(data => {
      let output;

      try {
        output = YAML.safeLoad(data);
      } catch (ex) {
        output = null;
      }

      return {
        output: output,
        input: data
      };
    })
    .map(data =>  data)
    .map(data => {
      if (typeof data.output !== 'object' || data.output === null) {
        // see if the content is a collapsible
        const regex = /<-- (.*) -->([\S\s]*?)<-- END -->/gmi;
        const matches = regex.exec(data.input) || [];

        console.log('[data]: ', JSON.stringify(data.input, null, 2));

        if (matches.length > 0) {
          return {
            component: 'collapse',
            title: matches[1],
            content: markdown(/(?:<\+\+>)([\S\s]*?)(?:<\+\+>)/gmi, data.input) || data.input
          };
        } else {
          return {
            component: 'markdown',
            content: converter.makeHtml(data.input.replace(/^<-- (.*) -->|<-- END -->$/gmi, ''))
          };
        }
      } else {
        return {
          component: data.output.component,
          content: data.output
        };
      }
    })
    .map(data => data);
  }
};

TREE.directory.metadata = function (directory) {
  const data = FS.readFileSync(PATH.join(directory, 'index.json'));
  return JSON.parse(data);
};

TREE.file.metadata = function (context) {
  // hold the contents of the read operation
  //let contents;
  return YAML.front(context);
};

YAML.parse = function (text, name) {
  name = name || '__content';
  let re = /^(-{3}(?:\n|\r)([\w\W]+?)(?:\n|\r)-{3})?([\w\W]*)*/
    , results = re.exec(text)
    , conf = {}
    , yamlOrJson;

  if (yamlOrJson = results[2]) {
    if (yamlOrJson.charAt(0) === '{') {
      conf = JSON.parse(yamlOrJson);
    } else {
      conf = YAML.load(yamlOrJson);
    }
  }

  conf[name] = results[3] ? results[3] : '';

  return conf;
};

YAML.front = function (context, name) {
  let contents;
  if (FS.existsSync(context)) {
    contents = FS.readFileSync(context, 'utf8');
    if (contents instanceof Error) return contents;
    return YAML.parse(contents, name);
  } else if (Buffer.isBuffer(context)) {
    return YAML.parse(context.toString(), name);
  } else {
    return YAML.parse(context, name);
  }
};

const sanitize = (args, replacement = '/') => {
  if (args.length > 1)
    return args.join(replacement)
    .replace(/^\//, '')
    .replace(/$\//, '');
  return args;
};

TREE.distinct = function (myArr, prop) {
  return myArr !== null ? myArr.filter((obj, pos, arr) => {
    return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
  }) : [];
};

TREE.sort = function (a, b) {
  // if a.order is -1
  return a.order < 0
    // and b.order is -1
    ? b.order < 0
      // alphabetic sort
      ? a.title > b.title
      // if b.order is not -1
      // then lowest order
      : b.order > a.order
    // if a.order is not -1
    : b.order < 0
      // then the highest number wins
      ? a.order > b.order
      : b.order < a.order;
};

module.exports = TREE;
