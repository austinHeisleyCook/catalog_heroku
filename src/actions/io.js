/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 11/28/2018 1:29
 ******************************************************************************/
const
  output = require('@coderpillr/operators/output')
  , Files = require('@coderpillr/io/files')
;

const Filesystem = (site) => {
  return new Promise((resolve) => {
    // console output
    output(`[IO][${site}]`);
    // create the tree
    const filesystem = new Files(site || 'public');
    // build the filesystem
    return resolve(filesystem);
  });
};

module.exports = Filesystem;
