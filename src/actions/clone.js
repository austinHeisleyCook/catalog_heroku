/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 2/18/2018 8:56
 ******************************************************************************/
const
  rm        = require('rimraf')
  , output  = require('@coderpillr/operators/output')
  , nodegit = require('nodegit')
;

function clone(branch, folder) {
  // clear out the existing data
  return new Promise((resolve) => {
    rm(folder, () => {
      // console output
      output(`[CLONE][${branch}]`);
      // perform cloning operation
      action(branch, folder).then((result) => {
        return resolve(result);
      });
    });
  });
}

const action = (branch, folder) => {
  // setup git credentials
  const token        = 'GIT TOKEN';
  const cloneURL     = '$GIT CLONE URL';
  //const cloneURL     = `https://${token}:x-oauth-basic@github.com/${repo}`;
  const cloneOptions = {
    checkoutBranch: branch,
    fetchOpts     : {
      callbacks: {
        certificateCheck: () => {
          return 1;
        },
        credentials     : () => {
          return new nodegit.Cred.userpassPlaintextNew(token, 'x-oauth-basic');
        }
      }
    }
  };

  return nodegit.Clone(cloneURL, folder, cloneOptions);
};

module.exports = clone;
