/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 2/18/2018 8:56
 ******************************************************************************/
const
  nodegit = require('nodegit')
  , output = require('@coderpillr/operators/output')
;

function pull(branch, origin, folder) {
  return new Promise((resolve) => {
    // console output
    output(`[FETCH][${branch}]`);
    // perform git pull
    action(branch, origin, folder).then((result) => {
      return resolve(result);
    });
  });

  //action(branch, origin).done(done());
}

const action = (branch, origin, folder) => {
  // setup git credentials
  const token        = 'GIT TOKEN';
  return nodegit.Repository
                .open(folder)
                .then(repository => {
                  return repository.fetchAll({
                    callbacks: {
                      certificateCheck: () => {
                        return 1;
                      },
                      credentials     : () => {
                        return new nodegit.Cred.userpassPlaintextNew(token, 'x-oauth-basic');
                      }
                    }
                  }).then(() => {
                    return repository.mergeBranches(branch, origin);
                  });
                });
};

module.exports = pull;
