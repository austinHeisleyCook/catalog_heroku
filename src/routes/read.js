/*******************************************************************************
 * COPYRIGHT © 2018 @CODERPILLR
 * @author Ciel
 * @modified 2/18/2018 8:53
 ******************************************************************************/

const
  express      = require('express')
  , router     = express.Router()
  , Files      = require('@coderpillr/io/files')
  , Filesystem = require('@coderpillr/actions/io')
  , data       = require('@coderpillr/io/data')
;

router.get('/:site/read/:name(*{1,})', (req, res) => {
  const site  = req.params.site.length > 0 ? req.params.site : 'public';
  const files = new Files(site || 'public');
  // ensure the filesystem is loaded in the proper directory
  // and allow it to be searched
  const results = req.params.name.length > 0
                  ? files.search(data(site), 'url', req.params.name).data
                  : data(site);

  const content = files.read(
    results.extension
    ? results.path
    : results.content);

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(content, undefined, 2));
});

module.exports = router;
