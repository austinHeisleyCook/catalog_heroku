/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 2/18/2018 8:56
 ******************************************************************************/
const
  express  = require('express')
  , router = express.Router()
  , CLONE  = require('../actions/clone')
  , Filesystem = require('@coderpillr/actions/io')
;

router.get('/:site/clone', (req, res) => {
  Promise.all([
    CLONE('master', './public').then(() => Filesystem('public', './public/pages/')),
    CLONE('private', './private').then(() => Filesystem('private', './private/pages/'))
  ]).then(() => res.send('--- [/CLONE/] ---'));
});

module.exports = router;
