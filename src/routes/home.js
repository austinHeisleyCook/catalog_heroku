/*******************************************************************************
 * COPYRIGHT © 2018 @CODERPILLR
 * @author Ciel
 * @modified 2/18/2018 8:53
 ******************************************************************************/
const
  express               = require('express')
  , router                = express.Router()
;

router.get('/', (req, res) => {
  res.redirect(`/public/list/`);
});

module.exports = router;
