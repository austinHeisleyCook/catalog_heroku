/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 2/18/2018 8:56
 ******************************************************************************/
const
  express   = require('express')
  , router  = express.Router()
  , PULL = require('../actions/pull')
  , Filesystem = require('@coderpillr/actions/io')
;

router.get('/:site/fetch', (req, res) => {
  console.log('fetching from gitlab');
  Promise.all([
    PULL('master', 'origin/master', './public').then(() => Filesystem('public', './public/pages/')),
    PULL('private', 'origin/private', './private').then(() => Filesystem('private', './private/pages/'))
  ]).then(() => res.send('--- [/FETCH/] ---'));
});

module.exports = router;
