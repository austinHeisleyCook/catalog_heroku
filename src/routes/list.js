/*******************************************************************************
 * COPYRIGHT © 2018 @CODERPILLR
 * @author Ciel
 * @modified 2/18/2018 8:53
 ******************************************************************************/

const
  express      = require('express')
  , router     = express.Router()
  , Files      = require('@coderpillr/io/files')
  , Filesystem = require('@coderpillr/actions/io')
  , distinct   = require('@coderpillr/io/distinct')
  , sort       = require('@coderpillr/io/sort')
  , data       = require('@coderpillr/io/data')
;

router.get('/:site/list/:name(*{1,})', (req, res) => {
  const site  = req.params.site.length > 0 ? req.params.site : 'public';
  const files = new Files(site || 'public');
  console.log('[list][site]: ', site);
  console.log('[list][url]: ', req.params);
  // ensure the filesystem is loaded in the proper directory
  // and allow it to be searched
  const results = req.params.name.length > 0
                  ? files.search(data(site), 'url', req.params.name).data
                  : data(site);

  // if the results have no items, search the parent
  if (results.items === undefined) {
    results.items = files.search(data(site), 'url', results.parent).data.items;
  }
  // filter out the items so only distinct ones remain
  results.items = distinct(results.items, 'url').sort(sort);

  res.setHeader('Content-Type', 'application/json');
  res.send(JSON.stringify(results, undefined, 2));

});

module.exports = router;
