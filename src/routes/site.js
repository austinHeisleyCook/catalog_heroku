/*******************************************************************************
 * COPYRIGHT 2018 @CODERPILLR
 * @author Ciel
 * @modified 3/19/2018 0:39
 ******************************************************************************/
const
  express  = require('express')
  , router = express.Router()
;

router.get('/:site/', (req, res) => {
  res.redirect(`/public/list/`);
});

module.exports = router;
