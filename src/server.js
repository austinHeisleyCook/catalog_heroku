/*******************************************************************************
 * COPYRIGHT © 2018 @CODERPILLR
 * @author Ciel
 * @modified 2/18/2018 8:53
 ******************************************************************************/
const
  express       = require('express')
  , cors        = require('cors')
  , compression = require('compression')
  , path        = require('path')
  // ------------------
  , server      = express()
  // ------------------
  , port        = require('@coderpillr/config/port')
  , onError     = require('@coderpillr/config/error')
  , onListening = require('@coderpillr/config/listening')
  // ------------------
  , home        = require('./routes/home')
  , site        = require('./routes/site')
  , list        = require('./routes/list')
  , read        = require('./routes/read')
  , clone       = require('./routes/clone')
  , fetch       = require('./routes/fetch')
;


const Startup = () => {
  // the current requesting host
  let host = '';
  server.set('port', process.env.PORT || port);

  /**
   * Record the host of the request
   */
  server.use((req, res, next) => {
    host = req.get('host');
    next();
  });

  // specify to use compression
  server.use(compression());
  // server.use(compression((req, res) => {
  //   if (req.headers['x-no-compression']) {
  //     // don't compress responses with this request header
  //     return false;
  //   }
  //
  //   // fallback to standard filter function
  //   return compression.filter(req, res);
  // }));
  // specify allowed cors domains
  const whitelist = [
    // PUT ALL OF THE URLS THAT WILL TRY TO ACCESS THE
    // FILES HERE AS A STRING ARRAY
  ];
  // specify to use cors
  server.use(cors({
    origin        : whitelist,
    methods       : ['*'],
    allowedHeaders: ['*'],
    exposedHeaders: ['*']
  }));
  // specify the path to static files
  server.use(express.static(path.join(__dirname, '../public/pages')));
  server.use(express.static(path.join(__dirname, '../private/pages')));

  server.get('/', home);
  server.get('/:site', site);
  server.get('/:site/clone', clone);
  server.get('/:site/fetch', fetch);
  server.get('/:site?/read/:name(*{1,})', read);
  server.get('/:site?/list/:name(*{1,})', list);

  // specify event handlers
  server.on('error', onError);
  server.on('listening', onListening);


  server.use((req, res, next) => {
    const err  = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  server.use((err, req, res) => {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error   = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
  });

  server.listen(server.get('port'), onListening);
};

module.exports = Startup;
