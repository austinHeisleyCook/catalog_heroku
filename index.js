/*******************************************************************************
 * COPYRIGHT © 2018 @CODERPILLR
 * @author Ciel
 * @modified 2/17/2018 11:58
 ******************************************************************************/
require('module-alias/register');
const
  http         = require('http')
  // CRON job can be used to keep the system alive or perform
  // scheduled tasks and maintenance.
  , CRON       = require('cron').CronJob
  , STARTUP    = require('./src/server')
  , CLONE      = require('./src/actions/clone')
  , PULL       = require('./src/actions/pull')
  , TREE       = require('./src/actions/tree')
  , Filesystem = require('@coderpillr/actions/io')
;

console.log('starting up: [REPOSITORY] -b master');

Promise.all([
  CLONE('master', './public').then(() => Filesystem('public', './public/pages/')),
  CLONE('private', './private').then(() => Filesystem('private', './private/pages/'))
]).then(() => STARTUP());


new CRON({
  // # .---------------- minute (0 - 59)
  // # |  .------------- hour (0 - 23)
  // # |  |  .---------- day of month (1 - 31)
  // # |  |  |  .------- month (1 - 12) OR jan,feb,mar,apr ...
  // # |  |  |  |  .---- day of week (0 - 6) (Sunday=0 or 7) OR sun,mon,tue,wed,thu,fri,sat
  // # |  |  |  |  |
  // # *  *  *  *  * user-name  command to be executed
  cronTime : '*/1 * * * *',
  start    : true,
  timeZone : 'America/Los_Angeles',
  runOnInit: false,
  onTick   : () => {
    Promise.all([
      PULL('master', 'origin/master', './public').then(() => Filesystem('public', './public/pages/')),
      PULL('private', 'origin/private', './private').then(() => Filesystem('private', './private/pages/'))
    ]);
  }
});

